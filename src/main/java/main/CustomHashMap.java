package main;


import java.util.HashMap;

/**
 * Для простоты
 *     - key и value это String
 *     - сделаны основные операция вставка, поиск, удаление
 *
 * Для разрешения коллизий используется метод цепочек
 */
public class CustomHashMap
{
    // Хранилище ключей(хэшей)-значений (и цепочек)
    Element[] dataStore;

    // конструктор - подготовка массива для ХэшМап
    // хэш ограничю, например, 100 значениеями
    public CustomHashMap()
    {
        this.dataStore = new Element[100];
    }

    // добавление нового значения в ХэшМап
    public void addValueByKey(String key, String value)
    {
        int index = getIndexForKey(key);

        // проверка есть ли что-нибудь уже по этому индексу
        Element element = dataStore[index];
        if (element == null)
        {
            element = new Element(key, value);
            dataStore[index] = element;
        }
        else
        {
            Element nextElement = new Element(key, value);
            Element lastElement = searchLastElement(element, nextElement);

            // добавить в конец последнего элемента. Если null - тогда был уже обнаружен такой элемент - делать ничего не надо
            if (lastElement != null)
            {
                lastElement.setNextElement(nextElement);
                nextElement.setPrevElement(lastElement);
            }
        }
    }

    // поиск значения по ключу
    public String findValueByKey(String key)
    {
        int index = getIndexForKey(key);

        Element element = dataStore[index];

        if (element == null)
        {
            return null;
        }
        else
        {
            Element searchedElement = searchForElement(element, key);

            if(searchedElement != null)
            {
                return searchedElement.getValue();
            }
            else
            {
                return null;
            }
        }
    }

    // удаление значения по ключу
    public Boolean deleteValueByKey(String key)
    {
        int index = getIndexForKey(key);

        Element element = dataStore[index];

        if (element == null)
        {
            return Boolean.TRUE;
        }
        else
        {
            Boolean isSuccess = deleteElementFromChain(element, key, index);

            return isSuccess;
        }
    }

    // Рекурсивный поиск последнего элемента в цепочке
    private Element searchLastElement(Element element, Element forAdd)
    {
        Element nextElement = element.getNextElement();

        // если такой элемент уже есть - ничего не делать вернуь null
        if (forAdd.getValue().equals(element.getValue()) && forAdd.getKey().equals(element.getKey()))
        {
            return null;
        }

        // если такой элемент c таким ключем уже есть, а значение другое - обновить и вернуть null
        if ( (!forAdd.getValue().equals(element.getValue())) && (forAdd.getKey().equals(element.getKey())) )
        {
            element.setValue(forAdd.getValue());
            return null;
        }

        if(nextElement != null)
        {
            return searchLastElement(nextElement, forAdd);
        }

        else
        {
            return element;
        }

    }


    // Рекурсивный поиск необходимого элемента в цепочке
    private Element searchForElement(Element element, String forSearchKey)
    {
        // если такой элемент уже есть - ничего не делать вернуь null
        if (element.getKey().equals(forSearchKey))
        {
            return element;
        }

        Element nextElement = element.getNextElement();

        if(nextElement != null)
        {
            return searchForElement(nextElement, forSearchKey);
        }

        else
        {
            return null;
        }

    }


    // рассчет индекса в массиве на основе рассчитанного хэш значения
    private int getIndexForKey(String key)
    {
        int index = key.hashCode();

        // в массиве не бывает отрицательных индексов, а хэш их может дать, поэтому - инверсия
        if (index < 0)
        {
            index = index * -1;
        }

        // для простоты сократить количество хэшей до 100
        index = index % 100;

        return index;
    }

    // Удаление из цепочки ненужного больше объекта
    private Boolean deleteElementFromChain(Element element, String key, int index)
    {
       Element searchedElement = searchForElement(element, key);

       Element prevElement = searchedElement.getPrevElement();

       Element nextElement = searchedElement.getNextElement();

       if (prevElement == null && nextElement != null)
       {
           dataStore[index] = nextElement;
       }

       if (nextElement == null && prevElement != null)
       {
           prevElement.setNextElement(null);
       }

        if (nextElement == null && prevElement == null)
        {
            dataStore[index] = null;
        }

        if (nextElement != null && prevElement != null)
        {
            prevElement.setNextElement(nextElement);
            nextElement.setPrevElement(prevElement);
        }

        // очистка ненужного объекта
        searchedElement = null;

       return Boolean.TRUE;
    }

}