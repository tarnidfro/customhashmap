package main;

import java.util.Scanner;

public class ShowCustomHashMap {

    public static void main(String[] args)
    {
        String key = null;
        String value = null;

        Scanner scanner = new Scanner(System.in);
        CustomHashMap myHash = new CustomHashMap();

        String option;

        for(;;)
        {
            System.out.println("искать - 1; добавлять - 2; удалять 3");
            option = scanner.nextLine();
            if ( option.equals("2"))
            {

                System.out.println("добавить ключ");
                key = scanner.nextLine();

                System.out.println("добавить значение для ключа");
                value = scanner.nextLine();

                myHash.addValueByKey(key, value);
            }

            if (option.equals("1"))
            {

                System.out.println("искомый ключ");
                key = scanner.nextLine();


                System.out.println("искомое значение для ключа: " + myHash.findValueByKey(key));
            }


            if (option.equals("3"))
            {
                System.out.println("удалить ключ");
                key = scanner.nextLine();


                System.out.println("результат удаления ключа: " + myHash.deleteValueByKey(key));
            }


        }

    }

}
