package main;

/**
 * Для разрешения коллизий используется двусвязынй список
 * Для простоты содержит только необходимые элементы
 */
public class Element
{
    String value;

    String key;

    Element nextElement;

    Element prevElement;

    public Element(String key, String value)
    {
        this.key = key;
        this.value = value;
    }

    public String getKey()
    {
        return key;
    }

    public Element getPrevElement()
    {
        return prevElement;
    }

    public void setPrevElement(Element prevElement)
    {
        this.prevElement = prevElement;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public Element getNextElement()
    {
        return nextElement;
    }

    public void setNextElement(Element nextElement)
    {
        this.nextElement = nextElement;
    }
}
